import * as fs from 'fs';
import * as path from 'path'; // Import the 'path' module to handle paths

async function checkIfFileExists(filePath: string): Promise<boolean> {
    return fs.existsSync(filePath);
}

export async function checkAndMove(imageId: string): Promise<boolean> {
    const sourcePath = path.join(__dirname, '..', '..', '..', 'registry-auth', 'storage', 'temporary', imageId);
    const targetPath = path.join(__dirname, '..', '..', '..', 'registry-auth', 'storage', 'persistent', imageId);
    console.log(sourcePath);
    if (checkIfFileExists(sourcePath)) {
        try {
            await fs.promises.rename(sourcePath, targetPath);
            return true;
        } catch (error) {
            console.error('Error moving file:', error);
            return false;
        }
    } else {
        return false;
    }
}
