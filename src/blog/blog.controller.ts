import { Controller, Get, Post, Body, Patch, Param, Delete, BadRequestException, Req, UseGuards, UnauthorizedException, ParseIntPipe, UseInterceptors, UploadedFile, Res, NotFoundException, Version } from '@nestjs/common';
import { BlogService } from './blog.service';
import { BlogRequestDto } from './dto/blog-request.dto';
import { ApiBearerAuth, ApiBody, ApiConsumes, ApiTags } from '@nestjs/swagger';
import { FileInterceptor } from '@nestjs/platform-express';
// import {v4 as uuidv4} from 'uuid';
// import { diskStorage } from 'multer';
// // import { multerConfig } from '../../config/multer.config';
import { BlogRequestDto2 } from './dto/blog-request-2.dto';
import { createReadStream, statSync } from 'fs';
import { join } from 'path';
import { Observable, of } from 'rxjs';
import { MessagePattern } from '@nestjs/microservices';

@Controller('blog')
@ApiTags('Blog')
export class BlogController {
  constructor(private readonly blogService: BlogService) {}


  @MessagePattern('createBlog')
  create(data) {
    console.log("BRO PLEASE 2");
    return this.blogService.create(data.userId, data.blogRequestDto);
  }

  @MessagePattern('findAllBlog')
  findAll(sort: string) {
    return this.blogService.findAll(sort);
  }
  
  @MessagePattern('getBlogsByUser')
  getAllBlogsByUser(data) {
    return this.blogService.getAllBlogsByUser(data.id, data.sort);
  }

  @MessagePattern('findOneBlog')
  findOne(id: number) {
    return this.blogService.findOne(id);
  }

  @MessagePattern('updateBlog')
  updateBlog(data) {
    return this.blogService.update(data.isAdmin, data.id, data.userId, data.blogRequestDto);
  }

  @MessagePattern('deleteBlog')
  remove(data) {
    this.blogService.remove(data.isAdmin, data.id, data.userId);
  }
}

