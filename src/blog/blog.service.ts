import {Inject, Injectable, NotFoundException, UnauthorizedException, forwardRef} from '@nestjs/common';
import { BlogRepository } from './blog.repository';
import { BlogRequestDto2 } from './dto/blog-request-2.dto';
import { join } from 'path';
import { createReadStream, statSync } from 'fs';
import { Cron } from '@nestjs/schedule';
import { RpcException } from '@nestjs/microservices';

@Injectable()
export class BlogService {
  
  constructor(
    private blogsRepository: BlogRepository,
  ) {}
  
  async create(userId: number, blogRequestDto: BlogRequestDto2) {
    // console.log("masuk service");
    return this.blogsRepository.create(userId, blogRequestDto);
  }

  findAll(sort: string){
    console.log(sort);
    return this.blogsRepository.findAll(sort);
  }

  async findOne(id: number) {
    const result = await this.blogsRepository.getBlogById(id);
    if (!result) {
      throw new RpcException(
        new NotFoundException("Blog not found")
      );
    }
    return result;
  }

  async remove(isAdmin: boolean,id: number, userId: number){
    
    const blog = await this.blogsRepository.getBlogById2(id);
    if (!blog) {
      throw new RpcException(
        new NotFoundException("Blog not found")
      );
    }
    if (blog.userId !== userId && !isAdmin) {
      throw new RpcException(
        new UnauthorizedException("You don't have permission to delete this blog")
      );
    }

    const result = await this.blogsRepository.delete(id);
    return "Deleted Blog with id " + id.toString();
    
  }

  async update(isAdmin: boolean, id: number, userId: number, blogRequestDto: BlogRequestDto2) {

    const blog = await this.blogsRepository.getBlogById2(id);
    if (!blog) {
      console.log("MASA MASUK SINI");
      throw new RpcException(
        new NotFoundException("Blog not found")
      );
    }

    if (blog.userId !== userId && !isAdmin) {
      throw new RpcException(
        new UnauthorizedException("You don't have permission to update this blog")
      );
    }

    await this.blogsRepository.update(id, blogRequestDto);
    return await this.blogsRepository.getBlogById2(id);
  }

  async removeByUserId(id: number) {
    const blogs = await this.blogsRepository.getAllBlogsByUser(id, "ASC");
      for (const blog of blogs) {
        await this.blogsRepository.delete(blog.id);
      }
  }

  async getAllBlogsByUser(id: number, sort: string) {
    return await this.blogsRepository.getAllBlogsByUser(id, sort);
  }

  // async findOneUsername(username: string) {
  //   try {
  //     const user = await this.userService.findOneUsername(username);
  //     return await this.blogsRepository.getAllBlogsByUser(user.id);
  //   } catch (error) {
  //     throw error;
  //   }
  // }

  @Cron('*/1 * * * *')
  async publishScheduledBlogs() {
    console.log("REFRESH PAK");
    await this.blogsRepository.publishAllUnpublishedBlogs();
  }

}
