import { Module, forwardRef } from '@nestjs/common';
import { BlogService } from './blog.service';
import { BlogController } from './blog.controller';
import { Blog } from './entities/blog.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BlogRepository } from './blog.repository';
import { PassportModule } from '@nestjs/passport';
@Module({
  controllers: [BlogController],
  providers: [BlogService, BlogRepository],
  imports: [TypeOrmModule.forFeature([Blog]), PassportModule],
  exports: [BlogService, BlogRepository]
})
export class BlogModule {}
