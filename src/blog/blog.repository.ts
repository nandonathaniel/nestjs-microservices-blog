import { Injectable} from '@nestjs/common';
import { BlogRequestDto} from './dto/blog-request.dto';
import {Blog} from './entities/blog.entity'
import { InjectRepository } from '@nestjs/typeorm';
import { DataSource, Repository } from 'typeorm';
import { BlogRequestDto2 } from './dto/blog-request-2.dto';
// import env from '../../config/env';
// import { checkAndMove} from '../../utils/checkIfFileExists';
import { BlogType } from './enums/type.enum';
import { checkAndMove } from '../utils/checkIfFileExists';

@Injectable()
export class BlogRepository {
    @InjectRepository(Blog)
    private blogsRepository: Repository<Blog>;

    constructor(db: DataSource) {
        this.blogsRepository = db.getRepository(Blog);
    }

    async clear() {
        console.log("blog ke clear");
        const allBlogs = await this.blogsRepository.find();
        await this.blogsRepository.remove(allBlogs);
    }

    delete(id: number) {
        return this.blogsRepository.softDelete(id);
    }

    findAll(sort: string) {
        if(sort == 'asc'){
            return this.blogsRepository.find({
                where: {
                    isPublished: true,
                },
                order : {
                    createdDate : 'asc'
                }
            });
        }
        if(sort == 'desc'){
            return this.blogsRepository.find({
                where: {
                    isPublished: true,
                },
                order : {
                    createdDate : 'desc'
                }
            });
        }
    }

    findAllUnpublishedBlogs() {
        return this.blogsRepository.find({
            where: {
                isPublished: false,
            },
        });
    }

    save(blog: Blog) {
        this.blogsRepository.save(blog);
    }

    async create(userId: number,blogRequestDto: BlogRequestDto2) {
        let theUrl = null;
        if(blogRequestDto.imageFileName) {
            const exists = await checkAndMove(blogRequestDto.imageFileName);

            if(!exists) {
                throw new Error('Image not found in the "temporary" folder.');
            }

            theUrl = "http://localhost:3010" + "/persistent/" + blogRequestDto.imageFileName;
        }

        const { type, ...blogData } = blogRequestDto;

        const blog = this.blogsRepository.create({
            userId: userId,
            ...blogData,
            imageUrl: theUrl,
            createdDate: new Date(),
            isPublished: blogRequestDto.type !== BlogType.Scheduled,
        });

        return this.blogsRepository.save(blog);
    }

    // create2(userId: number, title: string, content: string, image: string) {
    //     const blog = this.blogsRepository.create({
    //         userId: userId,
    //         title: title,
    //         content: content,
    //         image: "http://localhost:" + env.PORT + "/" + image || null,
    //         createdDate: new Date()
    //     });
    //     return this.blogsRepository.save(blog);
    // }

    async update(id: number, blogRequestDto: BlogRequestDto2) {
        let theUrl = null;
        if(blogRequestDto.imageFileName) {
            const exists = await checkAndMove(blogRequestDto.imageFileName);

            if(!exists) {
                throw new Error('Image not found in the "temporary" folder.');
            }

            theUrl = "http://localhost:3010" + "/persistent/" + blogRequestDto.imageFileName;
        }
        
        const { type, ...blogData } = blogRequestDto;

        const updateData = {
            ...blogData,
            createdDate: new Date()
        }
        const simpan = blogRequestDto.type ? { ...updateData, isPublished: blogRequestDto.type !== BlogType.Scheduled } : updateData;

        const result = await this.blogsRepository.update(
            { id },
            blogRequestDto.imageFileName ? { ...simpan, imageUrl: theUrl } : simpan
        );
        return result;
    }

    async getAllBlogsByUser(id: number, sort: string) {
        if(sort == 'asc'){
            return this.blogsRepository.find({
                where: {
                    isPublished: true,
                },
                order: {
                    createdDate: 'asc'
                }
            });
        }
        if(sort == 'desc'){
            return await this.blogsRepository.find({
                where: {
                  userId: id,
                  isPublished: true
                },
                order: {
                    createdDate: 'desc'
                }
            });
        }
    }

    getBlogById(id: number) {
        return this.blogsRepository.findOne({
            where: {
                id: id,
                isPublished: true
            }
        });
    }

    getBlogById2(id: number) {
        return this.blogsRepository.findOne({
            where: {
                id: id,
            }
        });
    }

    async publishAllUnpublishedBlogs() {
        await this.blogsRepository.update({ isPublished: false }, { isPublished: true });
    }
}

