import { ApiProperty } from "@nestjs/swagger";
import { BlogType } from "../enums/type.enum";

export class BlogRequestDto2 {
    @ApiProperty({
        example : "Example Title"
    })
    title: string;
    @ApiProperty({
        example : "Example Content"
    })
    content: string;
    @ApiProperty({
        example : "indonesiaarenae51d5c61-f78e-43d0-9fa1-5f96c7a9e590.jpg"
    })
    imageFileName: string;

    @ApiProperty({ 
        example: BlogType.General
    })
    type: BlogType;
}
