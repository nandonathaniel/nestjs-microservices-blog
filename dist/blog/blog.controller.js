"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BlogController = void 0;
const common_1 = require("@nestjs/common");
const blog_service_1 = require("./blog.service");
const swagger_1 = require("@nestjs/swagger");
const microservices_1 = require("@nestjs/microservices");
let BlogController = exports.BlogController = class BlogController {
    constructor(blogService) {
        this.blogService = blogService;
    }
    create(data) {
        console.log("BRO PLEASE 2");
        return this.blogService.create(data.userId, data.blogRequestDto);
    }
    findAll(sort) {
        return this.blogService.findAll(sort);
    }
    getAllBlogsByUser(data) {
        return this.blogService.getAllBlogsByUser(data.id, data.sort);
    }
    findOne(id) {
        return this.blogService.findOne(id);
    }
    updateBlog(data) {
        return this.blogService.update(data.isAdmin, data.id, data.userId, data.blogRequestDto);
    }
    remove(data) {
        this.blogService.remove(data.isAdmin, data.id, data.userId);
    }
};
__decorate([
    (0, microservices_1.MessagePattern)('createBlog'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], BlogController.prototype, "create", null);
__decorate([
    (0, microservices_1.MessagePattern)('findAllBlog'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], BlogController.prototype, "findAll", null);
__decorate([
    (0, microservices_1.MessagePattern)('getBlogsByUser'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], BlogController.prototype, "getAllBlogsByUser", null);
__decorate([
    (0, microservices_1.MessagePattern)('findOneBlog'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", void 0)
], BlogController.prototype, "findOne", null);
__decorate([
    (0, microservices_1.MessagePattern)('updateBlog'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], BlogController.prototype, "updateBlog", null);
__decorate([
    (0, microservices_1.MessagePattern)('deleteBlog'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], BlogController.prototype, "remove", null);
exports.BlogController = BlogController = __decorate([
    (0, common_1.Controller)('blog'),
    (0, swagger_1.ApiTags)('Blog'),
    __metadata("design:paramtypes", [blog_service_1.BlogService])
], BlogController);
//# sourceMappingURL=blog.controller.js.map