import { BlogService } from './blog.service';
export declare class BlogController {
    private readonly blogService;
    constructor(blogService: BlogService);
    create(data: any): Promise<import("./entities/blog.entity").Blog>;
    findAll(sort: string): Promise<import("./entities/blog.entity").Blog[]>;
    getAllBlogsByUser(data: any): Promise<import("./entities/blog.entity").Blog[]>;
    findOne(id: number): Promise<import("./entities/blog.entity").Blog>;
    updateBlog(data: any): Promise<import("./entities/blog.entity").Blog>;
    remove(data: any): void;
}
