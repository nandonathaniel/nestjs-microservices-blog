import { Blog } from './entities/blog.entity';
import { DataSource } from 'typeorm';
import { BlogRequestDto2 } from './dto/blog-request-2.dto';
export declare class BlogRepository {
    private blogsRepository;
    constructor(db: DataSource);
    clear(): Promise<void>;
    delete(id: number): Promise<import("typeorm").UpdateResult>;
    findAll(sort: string): Promise<Blog[]>;
    findAllUnpublishedBlogs(): Promise<Blog[]>;
    save(blog: Blog): void;
    create(userId: number, blogRequestDto: BlogRequestDto2): Promise<Blog>;
    update(id: number, blogRequestDto: BlogRequestDto2): Promise<import("typeorm").UpdateResult>;
    getAllBlogsByUser(id: number, sort: string): Promise<Blog[]>;
    getBlogById(id: number): Promise<Blog>;
    getBlogById2(id: number): Promise<Blog>;
    publishAllUnpublishedBlogs(): Promise<void>;
}
