"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BlogRepository = void 0;
const common_1 = require("@nestjs/common");
const blog_entity_1 = require("./entities/blog.entity");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const type_enum_1 = require("./enums/type.enum");
const checkIfFileExists_1 = require("../utils/checkIfFileExists");
let BlogRepository = exports.BlogRepository = class BlogRepository {
    constructor(db) {
        this.blogsRepository = db.getRepository(blog_entity_1.Blog);
    }
    async clear() {
        console.log("blog ke clear");
        const allBlogs = await this.blogsRepository.find();
        await this.blogsRepository.remove(allBlogs);
    }
    delete(id) {
        return this.blogsRepository.softDelete(id);
    }
    findAll(sort) {
        if (sort == 'asc') {
            return this.blogsRepository.find({
                where: {
                    isPublished: true,
                },
                order: {
                    createdDate: 'asc'
                }
            });
        }
        if (sort == 'desc') {
            return this.blogsRepository.find({
                where: {
                    isPublished: true,
                },
                order: {
                    createdDate: 'desc'
                }
            });
        }
    }
    findAllUnpublishedBlogs() {
        return this.blogsRepository.find({
            where: {
                isPublished: false,
            },
        });
    }
    save(blog) {
        this.blogsRepository.save(blog);
    }
    async create(userId, blogRequestDto) {
        let theUrl = null;
        if (blogRequestDto.imageFileName) {
            const exists = await (0, checkIfFileExists_1.checkAndMove)(blogRequestDto.imageFileName);
            if (!exists) {
                throw new Error('Image not found in the "temporary" folder.');
            }
            theUrl = "http://localhost:3010" + "/persistent/" + blogRequestDto.imageFileName;
        }
        const { type } = blogRequestDto, blogData = __rest(blogRequestDto, ["type"]);
        const blog = this.blogsRepository.create(Object.assign(Object.assign({ userId: userId }, blogData), { imageUrl: theUrl, createdDate: new Date(), isPublished: blogRequestDto.type !== type_enum_1.BlogType.Scheduled }));
        return this.blogsRepository.save(blog);
    }
    async update(id, blogRequestDto) {
        let theUrl = null;
        if (blogRequestDto.imageFileName) {
            const exists = await (0, checkIfFileExists_1.checkAndMove)(blogRequestDto.imageFileName);
            if (!exists) {
                throw new Error('Image not found in the "temporary" folder.');
            }
            theUrl = "http://localhost:3010" + "/persistent/" + blogRequestDto.imageFileName;
        }
        const { type } = blogRequestDto, blogData = __rest(blogRequestDto, ["type"]);
        const updateData = Object.assign(Object.assign({}, blogData), { createdDate: new Date() });
        const simpan = blogRequestDto.type ? Object.assign(Object.assign({}, updateData), { isPublished: blogRequestDto.type !== type_enum_1.BlogType.Scheduled }) : updateData;
        const result = await this.blogsRepository.update({ id }, blogRequestDto.imageFileName ? Object.assign(Object.assign({}, simpan), { imageUrl: theUrl }) : simpan);
        return result;
    }
    async getAllBlogsByUser(id, sort) {
        if (sort == 'asc') {
            return this.blogsRepository.find({
                where: {
                    isPublished: true,
                },
                order: {
                    createdDate: 'asc'
                }
            });
        }
        if (sort == 'desc') {
            return await this.blogsRepository.find({
                where: {
                    userId: id,
                    isPublished: true
                },
                order: {
                    createdDate: 'desc'
                }
            });
        }
    }
    getBlogById(id) {
        return this.blogsRepository.findOne({
            where: {
                id: id,
                isPublished: true
            }
        });
    }
    getBlogById2(id) {
        return this.blogsRepository.findOne({
            where: {
                id: id,
            }
        });
    }
    async publishAllUnpublishedBlogs() {
        await this.blogsRepository.update({ isPublished: false }, { isPublished: true });
    }
};
__decorate([
    (0, typeorm_1.InjectRepository)(blog_entity_1.Blog),
    __metadata("design:type", typeorm_2.Repository)
], BlogRepository.prototype, "blogsRepository", void 0);
exports.BlogRepository = BlogRepository = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [typeorm_2.DataSource])
], BlogRepository);
//# sourceMappingURL=blog.repository.js.map