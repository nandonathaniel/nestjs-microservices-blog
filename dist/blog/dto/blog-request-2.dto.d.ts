import { BlogType } from "../enums/type.enum";
export declare class BlogRequestDto2 {
    title: string;
    content: string;
    imageFileName: string;
    type: BlogType;
}
