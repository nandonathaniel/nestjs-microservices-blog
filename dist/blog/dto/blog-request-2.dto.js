"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BlogRequestDto2 = void 0;
const swagger_1 = require("@nestjs/swagger");
const type_enum_1 = require("../enums/type.enum");
class BlogRequestDto2 {
}
exports.BlogRequestDto2 = BlogRequestDto2;
__decorate([
    (0, swagger_1.ApiProperty)({
        example: "Example Title"
    }),
    __metadata("design:type", String)
], BlogRequestDto2.prototype, "title", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        example: "Example Content"
    }),
    __metadata("design:type", String)
], BlogRequestDto2.prototype, "content", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        example: "indonesiaarenae51d5c61-f78e-43d0-9fa1-5f96c7a9e590.jpg"
    }),
    __metadata("design:type", String)
], BlogRequestDto2.prototype, "imageFileName", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        example: type_enum_1.BlogType.General
    }),
    __metadata("design:type", String)
], BlogRequestDto2.prototype, "type", void 0);
//# sourceMappingURL=blog-request-2.dto.js.map