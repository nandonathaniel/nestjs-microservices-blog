export declare class BlogRequestDto {
    title: string;
    content: string;
}
