"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BlogService = void 0;
const common_1 = require("@nestjs/common");
const blog_repository_1 = require("./blog.repository");
const schedule_1 = require("@nestjs/schedule");
const microservices_1 = require("@nestjs/microservices");
let BlogService = exports.BlogService = class BlogService {
    constructor(blogsRepository) {
        this.blogsRepository = blogsRepository;
    }
    async create(userId, blogRequestDto) {
        return this.blogsRepository.create(userId, blogRequestDto);
    }
    findAll(sort) {
        console.log(sort);
        return this.blogsRepository.findAll(sort);
    }
    async findOne(id) {
        const result = await this.blogsRepository.getBlogById(id);
        if (!result) {
            throw new microservices_1.RpcException(new common_1.NotFoundException("Blog not found"));
        }
        return result;
    }
    async remove(isAdmin, id, userId) {
        const blog = await this.blogsRepository.getBlogById2(id);
        if (!blog) {
            throw new microservices_1.RpcException(new common_1.NotFoundException("Blog not found"));
        }
        if (blog.userId !== userId && !isAdmin) {
            throw new microservices_1.RpcException(new common_1.UnauthorizedException("You don't have permission to delete this blog"));
        }
        const result = await this.blogsRepository.delete(id);
        return "Deleted Blog with id " + id.toString();
    }
    async update(isAdmin, id, userId, blogRequestDto) {
        const blog = await this.blogsRepository.getBlogById2(id);
        if (!blog) {
            console.log("MASA MASUK SINI");
            throw new microservices_1.RpcException(new common_1.NotFoundException("Blog not found"));
        }
        if (blog.userId !== userId && !isAdmin) {
            throw new microservices_1.RpcException(new common_1.UnauthorizedException("You don't have permission to update this blog"));
        }
        await this.blogsRepository.update(id, blogRequestDto);
        return await this.blogsRepository.getBlogById2(id);
    }
    async removeByUserId(id) {
        const blogs = await this.blogsRepository.getAllBlogsByUser(id, "ASC");
        for (const blog of blogs) {
            await this.blogsRepository.delete(blog.id);
        }
    }
    async getAllBlogsByUser(id, sort) {
        return await this.blogsRepository.getAllBlogsByUser(id, sort);
    }
    async publishScheduledBlogs() {
        console.log("REFRESH PAK");
        await this.blogsRepository.publishAllUnpublishedBlogs();
    }
};
__decorate([
    (0, schedule_1.Cron)('*/1 * * * *'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], BlogService.prototype, "publishScheduledBlogs", null);
exports.BlogService = BlogService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [blog_repository_1.BlogRepository])
], BlogService);
//# sourceMappingURL=blog.service.js.map