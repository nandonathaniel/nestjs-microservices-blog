"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BlogType = void 0;
var BlogType;
(function (BlogType) {
    BlogType["General"] = "general";
    BlogType["Scheduled"] = "scheduled";
})(BlogType || (exports.BlogType = BlogType = {}));
//# sourceMappingURL=type.enum.js.map