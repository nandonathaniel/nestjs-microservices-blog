import { BlogRepository } from './blog.repository';
import { BlogRequestDto2 } from './dto/blog-request-2.dto';
export declare class BlogService {
    private blogsRepository;
    constructor(blogsRepository: BlogRepository);
    create(userId: number, blogRequestDto: BlogRequestDto2): Promise<import("./entities/blog.entity").Blog>;
    findAll(sort: string): Promise<import("./entities/blog.entity").Blog[]>;
    findOne(id: number): Promise<import("./entities/blog.entity").Blog>;
    remove(isAdmin: boolean, id: number, userId: number): Promise<string>;
    update(isAdmin: boolean, id: number, userId: number, blogRequestDto: BlogRequestDto2): Promise<import("./entities/blog.entity").Blog>;
    removeByUserId(id: number): Promise<void>;
    getAllBlogsByUser(id: number, sort: string): Promise<import("./entities/blog.entity").Blog[]>;
    publishScheduledBlogs(): Promise<void>;
}
