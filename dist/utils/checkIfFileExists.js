"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.checkAndMove = void 0;
const fs = require("fs");
const path = require("path");
async function checkIfFileExists(filePath) {
    return fs.existsSync(filePath);
}
async function checkAndMove(imageId) {
    const sourcePath = path.join(__dirname, '..', '..', '..', 'registry-auth', 'storage', 'temporary', imageId);
    const targetPath = path.join(__dirname, '..', '..', '..', 'registry-auth', 'storage', 'persistent', imageId);
    console.log(sourcePath);
    if (checkIfFileExists(sourcePath)) {
        try {
            await fs.promises.rename(sourcePath, targetPath);
            return true;
        }
        catch (error) {
            console.error('Error moving file:', error);
            return false;
        }
    }
    else {
        return false;
    }
}
exports.checkAndMove = checkAndMove;
//# sourceMappingURL=checkIfFileExists.js.map